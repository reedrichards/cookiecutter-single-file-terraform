#  {{ cookiecutter.project_name }}

- https://{{ cookiecutter.gitlab_domain }}/{{ cookiecutter.gitlab_account }}/{{ cookiecutter.project_name }}

## Setup and Configuration

### create an iam user for terraform:

```hcl
resource "aws_iam_user" "{{ cookiecutter.project_name }}" {
  name = "{{ cookiecutter.project_name }}"

  tags = {
    Project  = "{{ cookiecutter.project_name }}"
    Type  = "terraform"
  }
}

data "template_file" "{{ cookiecutter.project_name }}" {
  template = file("./policies/{{ cookiecutter.project_name }}.json")
} 
resource "aws_iam_user_policy" "{{ cookiecutter.project_name }}" {
  name = "{{ cookiecutter.project_name }}"
  user = aws_iam_user.{{ cookiecutter.project_name }}.name

  policy = data.template_file.{{ cookiecutter.project_name }}.rendered
}

```

configure {{ cookiecutter.gitlab_url }}/{{ cookiecutter.gitlab_account }}/{{ cookiecutter.project_name }}/-/settings/ci_cd
for with access key terraform user 

create access key https://console.aws.amazon.com/iam/home#/users/{{ cookiecutter.project_name }}?section=security_credentials

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

### push this repo to the new project on gitlab 

```shell
git init --initial-branch=main
git remote add origin git@{{ cookiecutter.gitlab_domain }}:{{ cookiecutter.gitlab_account}}/{{ cookiecutter.project_name}}.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### create gitlab token

navigate to [gitlab profile](https://{{ cookiecutter.gitlab_domain }}/{{ cookiecutter.gitlab_account }}/-/profile/personal_access_tokens) and create a new token

add the token to `.env` file as `TOKEN`

```
TOKEN=
```

### add gitlab project id to .env

```
PROJECT_ID=
```

intialize terraform

```shell
just init
```